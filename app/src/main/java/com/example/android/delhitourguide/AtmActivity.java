package com.example.android.delhitourguide;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class AtmActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.data_list);

        ArrayList<DataWord> dataWord = new ArrayList<DataWord>();
        dataWord.add(new DataWord("Canara Bank", "7/48, Malcha Marg, Chanakyapuri",R.drawable.atm_machine));
        dataWord.add(new DataWord("Canara Bank", "A3/12, Janakpuri, Near Chanan",R.drawable.atm_machine));
        dataWord.add(new DataWord("ICICI Bank", "Phelps Building, Inner Circle",R.drawable.atm_machine));
        dataWord.add(new DataWord("ICICI Bank", "H/2, Green Park Extension",R.drawable.atm_machine));
        dataWord.add(new DataWord("Bank of Baroda", "D-2/ 211, Munirka, Delhi 110 067",R.drawable.atm_machine));
        dataWord.add(new DataWord("Bank of Baroda", "L.I.C. Building, District Centre",R.drawable.atm_machine));
        dataWord.add(new DataWord("Punjab National Bank", "Alaknanda Shopping Complex",R.drawable.atm_machine));
        dataWord.add(new DataWord("Punjab National Bank", "Alaknanda VSC Tara Appt. Market",R.drawable.atm_machine));
        dataWord.add(new DataWord("Punjab National Bank", "D Block DDA Market, Anand Vihar",R.drawable.atm_machine));

        DataAdapter adapter = new DataAdapter(this, dataWord, R.color.color_atm);
        ListView listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(adapter);
    }
}
