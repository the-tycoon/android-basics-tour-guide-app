package com.example.android.delhitourguide;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class RestaurantsFragment extends Fragment {

    public RestaurantsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.data_list, container, false);

        ArrayList<DataWord> dataWord = new ArrayList<DataWord>();
        dataWord.add(new DataWord("Masala House", "Sunder Nagar, New Delhi", R.drawable.restaurant_food));
        dataWord.add(new DataWord("Lakhori - Haveli Dharampura", "Chandni Chowk, New Delhi", R.drawable.restaurant_dish));
        dataWord.add(new DataWord("Getafix", "Greater Kailash (GK) 1, New Delhi", R.drawable.restaurant_food));
        dataWord.add(new DataWord("China Garden", "Greater Kailash (GK) 2, New Delhi", R.drawable.restaurant_dish));
        dataWord.add(new DataWord("Tamasha", "Connaught Place, New Delhi", R.drawable.restaurant_dish));
        dataWord.add(new DataWord("House of Commons", "Connaught Place, New Delhi", R.drawable.restaurant_food));
        dataWord.add(new DataWord("Moti Mahal", "Daryaganj, New Delhi", R.drawable.restaurant_dish));
        dataWord.add(new DataWord("Lodi - The Garden Restaurant", "Lodhi Road, New Delhi", R.drawable.restaurant_food));

        DataAdapter adapter = new DataAdapter(getActivity(), dataWord, R.color.color_restaurant);
        ListView listView = (ListView) rootView.findViewById(R.id.list);
        listView.setAdapter(adapter);

        return rootView;
    }

}
