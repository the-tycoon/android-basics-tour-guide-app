package com.example.android.delhitourguide;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class HotelsFragment extends Fragment {

    public HotelsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.data_list, container, false);

        ArrayList<DataWord> dataWord = new ArrayList<DataWord>();
        dataWord.add(new DataWord("Hotel Cabana", "Paharganj, Delhi", R.drawable.hotel));
        dataWord.add(new DataWord("BB Palace-A Boutique Hotel", "Karol Bagh Area, Delhi", R.drawable.hotel));
        dataWord.add(new DataWord("The Park New Delhi", "Connaught Place, Delhi", R.drawable.hotel));
        dataWord.add(new DataWord("KK HOTEL", "Nehru Place, Delhi", R.drawable.hotel));
        dataWord.add(new DataWord("Hotel The Royal Plaza", "Connaught Place, Delhi", R.drawable.hotel));
        dataWord.add(new DataWord("Radisson Blu Hotel", "Dwarka, Delhi", R.drawable.hotel));
        dataWord.add(new DataWord("Hotel Hari Piorko", "Paharganj, Delhi", R.drawable.hotel));
        dataWord.add(new DataWord("Hotel Aman Continental", "Paharganj, Delhi", R.drawable.hotel));

        DataAdapter adapter = new DataAdapter(getActivity(), dataWord, R.color.colorPrimaryDark);
        ListView listView = (ListView) rootView.findViewById(R.id.list);
        listView.setAdapter(adapter);

        return rootView;
    }

}
