package com.example.android.delhitourguide;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class MetroActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.data_list);

        ArrayList<DataWord> dataWord = new ArrayList<DataWord>();
        dataWord.add(new DataWord("Adarsh Nagar metro station", "Near Adarsh Nagar", R.drawable.metro));
        dataWord.add(new DataWord("Akshardham metro station", "Opposite Akshardham Temple", R.drawable.metro));
        dataWord.add(new DataWord("Anand Vihar metro station", "Near Anand vihar Bus Station", R.drawable.metro));
        dataWord.add(new DataWord("Nirman Vihar metro station", "Oppsite V3S Mall", R.drawable.metro));
        dataWord.add(new DataWord("Rajiv Chowk metro station", "Cannaught Palace", R.drawable.metro));
        dataWord.add(new DataWord("Saket metro station", "Near Hauz Khas Village", R.drawable.metro));
        dataWord.add(new DataWord("Welcome", "Near Seelampur", R.drawable.metro));
        dataWord.add(new DataWord("Yamuna Bank metro station", "Yamuna Bank", R.drawable.metro));
        dataWord.add(new DataWord("Tagore Garden metro station", "Near Tagore Garden", R.drawable.metro));

        DataAdapter adapter = new DataAdapter(this, dataWord, R.color.color_metro);
        ListView listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(adapter);
    }
}
